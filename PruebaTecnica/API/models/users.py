from django.db import models

# Simple user model

class User(models.Model):
    user_name = models.CharField('Nombres', max_length=30)
    user_lastname = models.CharField('Apellidos', max_length=30)
    password = models.CharField('Contraseña', max_length=255)

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'
    
    def __str__(self):
        return f'{self.id}. {self.user_name} {self.user_lastname}'