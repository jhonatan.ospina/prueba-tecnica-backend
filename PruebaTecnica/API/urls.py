from django.urls import path
from .views.auth import AuthApi
from .views.user import UserApi

urlpatterns = [
    path('user', UserApi.as_view()),
    path('auth', AuthApi.as_view())
]