import jwt
import datetime
from django.conf import settings
from ..models.users import User

def decode_token(token: str):
    """ Returns user if token is valid else None """
    if not token or len(token.split(' ')) != 2 or token.split(' ')[0].lower() != 'bearer':
        return None
    try:
        payload = jwt.decode(token.split(' ')[1], settings.SECRET_KEY, algorithms='HS256')
        return payload['user']
    except jwt.InvalidTokenError:
        return None