from cerberus import Validator
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth.hashers import make_password

from ..models.users import User
from ..helpers.jwt import decode_token

class UserApi(APIView):

    def get(self, request):
        if not decode_token(request.headers.get('Authorization',None)):
            return Response({
                'error_message': 'Token de acceso inválido'
            }, status=status.HTTP_401_UNAUTHORIZED)
        return Response(User.objects.all().values('user_name','user_lastname'),status=status.HTTP_200_OK)
    
    def post(self, request):
        if not Validator({
            'user_name':{'required':True, 'type':'string'},
            'user_lastname':{'required':True, 'type':'string'},
            'password':{'required':True, 'type':'string'}
        }).validate(request.data):
            return Response({'error_message': 'Petición inválida'}, status=status.HTTP_400_BAD_REQUEST)
        
        if User.objects.filter(user_name=request.data['user_name'], user_lastname=request.data['user_lastname']):
            return Response({
                'error_message': 'El usuario ya existe'
            }, status=status.HTTP_409_CONFLICT)
        
        request.data['password'] = make_password(request.data['password'])

        user = User.objects.create(**request.data)
        return Response({"user_id":user.id}, status=status.HTTP_201_CREATED)