import jwt
from django.conf import settings
from cerberus import Validator
from django.contrib.auth.hashers import check_password
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from ..models.users import User

class AuthApi(APIView):

    def post(self, request):
        if not Validator({
            'user':{'required':True, 'type':'integer'},
            'password':{'required':True, 'type':'string'}
        }).validate(request.data):
            return Response({'error_message': 'Petición inválida'}, status=status.HTTP_400_BAD_REQUEST)
        
        user = User.objects.filter(id=request.data['user']).first()

        if not user or not check_password(request.data['password'],user.password):
            return Response({
                'error_message': 'Credenciales inválidas o usuario inexistente'
            }, status=status.HTTP_401_UNAUTHORIZED)
        
        token = jwt.encode({'user':user.id}, settings.SECRET_KEY,algorithm='HS256')
        
        return Response({
            'token':token,
            'user_name':user.user_name
        }, status=status.HTTP_200_OK)
        

